require('dotenv').config();
const express = require('express');
const axios = require('axios');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3358;

const apiUrl = process.env.DATA_URL;

const allowedOrigins = [
    'http://localhost:5173',
    'https://thejb.onrender.com'
];

app.use(cors({
    origin: allowedOrigins
}));

const fetchData = async () => {
    try {
        const response = await axios.get(apiUrl);

        const data = response.data;

        return data;
    } catch (error) {
        console.error('Error fetching data:', error);
        throw new Error('An error occurred while fetching data.');
    }
};

app.get('/', async (req, res) => {
    try {
        const data = await fetchData();
        res.json(data);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
