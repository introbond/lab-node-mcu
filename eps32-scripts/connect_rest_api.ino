#include <WiFi.h>
#include <HTTPClient.h>
#include "config.h"

const char *ssid = "wifi-ssid";
const char *password = "wifi-password";

const unsigned long interval = 60 * 60 * 1000;
unsigned long previousMillis = 0;

void setupWiFi() {
    Serial.begin(115200);
    delay(4000);

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println("Connecting to WiFi...");
    }
    Serial.println("Connected to WiFi");
}

String fetchData(String url) {
    HTTPClient http;
    http.begin(url);

    int httpCode = http.GET();
    String payload = "";

    if (httpCode > 0) {
        if (httpCode == HTTP_CODE_OK) {
            payload = http.getString();
        }
    }
    else {
        Serial.println("HTTP request failed");
    }

    http.end();
    return payload;
}

void printData(String data) {
    Serial.println(data);
}

void setup() {
    setupWiFi();
}

void loop() {
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;

        if (WiFi.status() == WL_CONNECTED) {
            String data = fetchData(API_URL);
            printData(data);
        }
    }
    // Your other non-blocking tasks can go here
}
