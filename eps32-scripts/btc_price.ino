#include <ArduinoJson.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define NTP_SERVER "pool.ntp.org"
#define UTC_OFFSET 7
#define UTC_OFFSET_DST 0

const char *ssid = "xxxxx";
const char *password = "xxxxx";

const char *apiEndpoint = "https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd";

const unsigned long interval = 1 * 60 * 1000;
unsigned long previousMillis = 0;

LiquidCrystal_I2C lcd(0x27, 16, 2);

String fetchData(String url)
{
    HTTPClient http;
    http.begin(url);

    int httpCode = http.GET();
    String payload = "";

    if (httpCode > 0)
    {
        if (httpCode == HTTP_CODE_OK)
        {
            payload = http.getString();
        }
    }
    else
    {
        Serial.println("HTTP request failed");
    }

    http.end();
    return payload;
}

void printLocalTime()
{
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo))
    {
        lcd.setCursor(0, 1);
        lcd.println("Connection Err");

        return;
    }

    lcd.setCursor(8, 0);
    lcd.println(&timeinfo, "%H:%M:%S");
}

void setup()
{
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT);

    lcd.init();
    lcd.backlight();
    lcd.setCursor(0, 0);
    lcd.print("Connecting to ");
    lcd.setCursor(0, 1);
    lcd.print("WiFi... ");

    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(250);
    }

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("BTC-USD: ");

    configTime(UTC_OFFSET, UTC_OFFSET_DST, NTP_SERVER);
}

void loop()
{
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval)
    {
        previousMillis = currentMillis;

        if (WiFi.status() == WL_CONNECTED)
        {
            String data = fetchData(apiEndpoint);

            // Parse the JSON data to get BTC price
            DynamicJsonDocument jsonDoc(256); // Adjust the buffer size
            deserializeJson(jsonDoc, data);

            float btcPrice = jsonDoc["bitcoin"]["usd"].as<float>();

            lcd.setCursor(0, 1);
            lcd.print(btcPrice, 0);
            lcd.setCursor(15, 1);
            lcd.print("$");

            digitalWrite(LED_BUILTIN, HIGH);
            delay(100);
            digitalWrite(LED_BUILTIN, LOW);
        }
    }

    printLocalTime();
    delay(1000);
}
